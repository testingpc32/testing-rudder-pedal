using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CheckControl : MonoBehaviour
{
    [SerializeField] private InputActionAsset inputActionAsset;
    private InputActionMap tRudder;
    private InputAction leftToeBrake;
    private InputAction rightToeBrake;

    [SerializeField] private Vector2 leftToe;
    [SerializeField] private Vector2 rightToe;

    private void Start()
    {
        tRudder = inputActionAsset.FindActionMap("TRudder");
        leftToeBrake = tRudder.FindAction("Left Toe Brake");
        rightToeBrake = tRudder.FindAction("Right Toe Brake");

        Debug.Log($"Left Toe: {leftToeBrake}, Right Toe: {rightToeBrake}");
    }

    private void Update()
    {
        Debug.Log($"Update Left Toe: {leftToeBrake.ReadValue<Vector2>()}, Right Toe: {rightToeBrake.ReadValue<Vector2>()}");
        leftToe = leftToeBrake.ReadValue<Vector2>();
        rightToe = rightToeBrake.ReadValue<Vector2>();
    }
}
