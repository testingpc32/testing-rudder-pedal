using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;

public class SaitekFlightProRudders : Joystick
{
    static SaitekFlightProRudders()
    {
        InputSystem.RegisterLayout<SaitekFlightProRudders>(
            matches: new InputDeviceMatcher()
                .WithInterface("HID")
                .WithCapability("vendorId", 0x1103)
                .WithCapability("productId", 0x46713));
    }

    [RuntimeInitializeOnLoadMethod]
    static void Init() { }
}
